father(joe,sally).
father(joe,bill).
father(fred,henry).
mother(mary,sally).
mother(mary,bill).
mother(sarah,henry).
parent(P,C) :- father(P,C).
parent(P,C) :- mother(P,C).
child(C,P) :- parent(P,C).
sibling(C1,C2) :- parent(P,C1),parent(P,C2),C1\=C2.
